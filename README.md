# Zipkin Server #

[Zipkin](http://zipkin.io/) is a distributed tracing system. It helps gather timing data needed to troubleshoot latency problems in microservice architectures. 
It manages both the collection and lookup of this data.

UI at http://localhost:9411
